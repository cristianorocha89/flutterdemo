import 'package:flutter/material.dart';

import '../moduls/EmployeeActionModule/view/employee_details.dart';
import '../moduls/EmployeeActionModule/view/employye_action.dart';

class ShowDialog {
  static showActionEmployeeDialog(
    BuildContext context, {
    bool? isEdit,
    bool? isDelete,
    EmployeeActionArgs? addEmployeeArgs,
  }) {
    showDialog(
        context: context,
        builder: (context) => EmployeesAction(
              isDelete: isDelete ?? false,
              isEdit: isEdit ?? false,
              args: addEmployeeArgs,
            ));
  }

  static showActionEmployeeDetailsDialog(
    BuildContext context, {
    EmployeeDetailsArgs? addEmployeeArgs,
  }) {
    showDialog(
        context: context,
        builder: (context) => EmployeeDetails(
              args: addEmployeeArgs,
            ));
  }
}
