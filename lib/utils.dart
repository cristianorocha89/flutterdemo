import 'package:flutter/foundation.dart';

//============================================================
// ** App Common Print**
//============================================================
void appPrint(dynamic str) {
  if (kDebugMode) {
    print(str);
  }
}
