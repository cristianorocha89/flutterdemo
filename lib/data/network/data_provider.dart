import 'package:demo/constants/demo_color.dart';
import 'package:dio/dio.dart';
import 'package:encrypt/encrypt.dart' as enc;
import 'package:get/get.dart' as getSnank;

import '../../config.dart';
import '../../moduls/EmployeeActionModule/model/employee_action_model.dart';
import '../../moduls/EmployeeActionModule/view/employye_action.dart';
import '../../moduls/EmployeesModule/model/employeesModel.dart';
import '../../moduls/EmployeesModule/view/employyesList.dart';
import '../../moduls/deleted_list_module/model/deleted_list_model.dart';
import '../../moduls/deleted_list_module/view/deleted_list.dart';
import '../../utils.dart';

class DataProvider {
  DataProvider({required Dio dio}) : _dio = dio;

  final Dio _dio;
  final iv = enc.IV.fromLength(8);
  final encrypter = enc.Encrypter(enc.Salsa20(enc.Key.fromLength(32)));

  Future<EmployeesModel?> getEmployeesList(
      EmployeesArgument employeesArgument) async {
    try {
      String url =
          "${Config.appUrl}/employees?page=${employeesArgument.page}&limit=${employeesArgument.limit}";
      Response response = await _dio.get(
        url,
      );
      appPrint(response.data);
      EmployeesModel res = EmployeesModel.fromJson(response.data);
      return res;
    } catch (e) {
      if (e is DioError) {
        getSnank.Get.snackbar("error", e.response?.data?['message'][0],
            backgroundColor: DemoColor.red);
      } else {
        appPrint(e);
      }
      Exception(e);
    }
    return null;
  }

  Future<DeletedEmployeesModel?> getDeletedEmployeesList(
      DeletedEmployeesArg deletedEmployeesArg) async {
    try {
      String url =
          "${Config.appUrl}/employees/deleted?page=${deletedEmployeesArg.page}&limit=${deletedEmployeesArg.limit}";
      Response response = await _dio.get(
        url,
      );
      appPrint(response.data);
      DeletedEmployeesModel res = DeletedEmployeesModel.fromJson(response.data);
      return res;
    } catch (e) {
      if (e is DioError) {
        getSnank.Get.snackbar("error", e.response?.data?['message'][0],
            backgroundColor: DemoColor.red);
      } else {
        appPrint(e);
      }
      Exception(e);
    }
    return null;
  }

  Future<EmployeeActionModel?> addEmployee(
      EmployeeActionArgs addEmployeeArgs) async {
    //Response? response;
    try {
      String url = "${Config.appUrl}/employees";
      Response response = await _dio.post(url, data: {
        "name": addEmployeeArgs.name,
        "email": addEmployeeArgs.email,
        "phoneNumber": "+381 ${addEmployeeArgs.phoneNumber}",
        "homeAddress": {
          "city": addEmployeeArgs.city,
          "ZIPCode": addEmployeeArgs.zipCode,
          "addressLine1": addEmployeeArgs.addressLine1,
          "addressLine2": addEmployeeArgs.addressLine2
        },
        "dateOfEmployment": addEmployeeArgs.dateOfEmployment,
        "dateOfBirth": addEmployeeArgs.dateOfBirth
      });
      appPrint(response.data);

      EmployeeActionModel res = EmployeeActionModel.fromJson(response.data);

      return res;
    } catch (e) {
      if (e is DioError) {
        getSnank.Get.snackbar("error", e.response?.data?['message'][0],
            backgroundColor: DemoColor.red);
      } else {
        appPrint(e);
      }
      Exception(e);
    }
    return null;
  }

  Future<EmployeeActionModel?> updateEmployee(
      EmployeeActionArgs addEmployeeArgs) async {
    try {
      String url = "${Config.appUrl}/employees/${addEmployeeArgs.employeeId}";
      Response response = await _dio.patch(url, data: {
        "name": addEmployeeArgs.name,
        "email": addEmployeeArgs.email,
        "phoneNumber": "+381 ${addEmployeeArgs.phoneNumber}",
        "homeAddress": {
          "city": addEmployeeArgs.city,
          "ZIPCode": addEmployeeArgs.zipCode,
          "addressLine1": addEmployeeArgs.addressLine1,
          "addressLine2": addEmployeeArgs.addressLine2
        },
        "dateOfEmployment": addEmployeeArgs.dateOfEmployment,
        "dateOfBirth": addEmployeeArgs.dateOfBirth
      });
      appPrint(response.data);
      EmployeeActionModel res = EmployeeActionModel.fromJson(response.data);
      return res;
    } catch (e) {
      if (e is DioError) {
        getSnank.Get.snackbar("error", e.response?.data?['message'][0],
            backgroundColor: DemoColor.red);
      } else {
        appPrint(e);
      }
      Exception(e);
    }
    return null;
  }

  Future<EmployeeActionModel?> deleteEmployee(
      EmployeeActionArgs addEmployeeArgs) async {
    try {
      String url =
          "${Config.appUrl}/employees/soft-delete/${addEmployeeArgs.employeeId}";
      Response response = await _dio.delete(url);
      appPrint(response.data);
      EmployeeActionModel res = EmployeeActionModel.fromJson(response.data);
      return res;
    } catch (e) {
      if (e is DioError) {
        getSnank.Get.snackbar("error", e.response?.data?['message'][0],
            backgroundColor: DemoColor.red);
      } else {
        appPrint(e);
      }
      Exception(e);
    }
    return null;
  }
}
