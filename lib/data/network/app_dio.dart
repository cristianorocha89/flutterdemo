import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../../config.dart';
import 'dio_adapter_stub.dart'
    if (dart.library.io) 'dio_adapter_mobile.dart'
    if (dart.library.js) 'dio_adapter_web.dart';

class AppDio with DioMixin implements Dio {
  AppDio._([BaseOptions? options]) {
    HttpClientAdapter client;
    if (kIsWeb) {
      client = getAdapter();
    } else {
      client = getAdapter();
    }

    options = BaseOptions(
      baseUrl: Config.appUrl,
      contentType: 'application/json;charset=UTF-8',
      connectTimeout: 30000,
      sendTimeout: 30000,
      receiveTimeout: 30000,
    );

    this.options = options;

    if (kDebugMode) {
      // Local Log
      interceptors.add(LogInterceptor(responseBody: true, requestBody: true));
    }

    httpClientAdapter = client;
  }

  static Dio getInstance() => AppDio._();
}
