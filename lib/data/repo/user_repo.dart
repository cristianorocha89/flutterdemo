import 'package:demo/data/repo/user_repo_impl.dart';

import '../../moduls/EmployeeActionModule/model/employee_action_model.dart';
import '../../moduls/EmployeeActionModule/view/employye_action.dart';
import '../../moduls/EmployeesModule/model/employeesModel.dart';
import '../../moduls/EmployeesModule/view/employyesList.dart';
import '../../moduls/deleted_list_module/model/deleted_list_model.dart';
import '../../moduls/deleted_list_module/view/deleted_list.dart';
import '../network/app_dio.dart';
import '../network/data_provider.dart';

class UserRepo {
  static UserRepository getInstance() {
    return UserRepoImpl(
      dataProvider: DataProvider(
        dio: AppDio.getInstance(),
      ),
    );
  }
}

abstract class UserRepository {
  Future<EmployeesModel?> getEmployeesList(EmployeesArgument employeesArgument);

  Future<EmployeeActionModel?> addEmployees(EmployeeActionArgs addEmployeeArgs);

  Future<EmployeeActionModel?> updateEmployees(
      EmployeeActionArgs addEmployeeArgs);

  Future<EmployeeActionModel?> deleteEmployees(
      EmployeeActionArgs addEmployeeArgs);

  Future<DeletedEmployeesModel?> getDeletedEmployees(
      DeletedEmployeesArg deletedEmployeesArg);
}
