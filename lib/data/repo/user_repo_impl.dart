import 'package:demo/data/repo/user_repo.dart';
import 'package:demo/moduls/deleted_list_module/model/deleted_list_model.dart';
import 'package:demo/moduls/deleted_list_module/view/deleted_list.dart';

import '../../moduls/EmployeeActionModule/model/employee_action_model.dart';
import '../../moduls/EmployeeActionModule/view/employye_action.dart';
import '../../moduls/EmployeesModule/model/employeesModel.dart';
import '../../moduls/EmployeesModule/view/employyesList.dart';
import '../network/data_provider.dart';

class UserRepoImpl extends UserRepository {
  UserRepoImpl({required DataProvider dataProvider})
      : _dataProvider = dataProvider;

  final DataProvider _dataProvider;

  @override
  Future<EmployeesModel?> getEmployeesList(
      EmployeesArgument employeesArgument) {
    return _dataProvider.getEmployeesList(employeesArgument);
  }

  @override
  Future<EmployeeActionModel?> addEmployees(
      EmployeeActionArgs addEmployeeArgs) {
    return _dataProvider.addEmployee(addEmployeeArgs);
  }

  @override
  Future<EmployeeActionModel?> updateEmployees(
      EmployeeActionArgs updateEmployeeArgs) {
    return _dataProvider.updateEmployee(updateEmployeeArgs);
  }

  @override
  Future<EmployeeActionModel?> deleteEmployees(
      EmployeeActionArgs deleteEmployeeArgs) {
    return _dataProvider.deleteEmployee(deleteEmployeeArgs);
  }

  @override
  Future<DeletedEmployeesModel?> getDeletedEmployees(
      DeletedEmployeesArg deletedEmployeesArg) {
    return _dataProvider.getDeletedEmployeesList(deletedEmployeesArg);
  }
}
