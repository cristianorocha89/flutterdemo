import 'package:demo/constants/demo_string.dart';
import 'package:demo/helpers/common.dart';
import 'package:demo/router.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:responsive_framework/responsive_wrapper.dart';
import 'package:responsive_framework/utils/scroll_behavior.dart';

import 'moduls/EmployeesModule/view/employyesList.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
//============================================================
// ** GetMaterial Build Cycle **
//============================================================
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      navigatorKey: navigatorKey,
      title: DemoString.appName,
      debugShowCheckedModeBanner: false,
      initialRoute: EmployeesList.routeName,
      getPages: Routes.routes(),
      unknownRoute: Routes.unDefined(),
      builder: EasyLoading.init(
        builder: (context, child) => ResponsiveWrapper.builder(
          ClampingScrollWrapper.builder(context, child!),
          maxWidth: MediaQuery.of(context).size.width,
          minWidth: 1000,
          defaultScale: true,
          breakpoints: [
            const ResponsiveBreakpoint.autoScale(1000, name: TABLET),
            const ResponsiveBreakpoint.resize(1200, name: DESKTOP),
            const ResponsiveBreakpoint.resize(600, name: MOBILE),
            const ResponsiveBreakpoint.autoScale(2460, name: "4K"),
          ],
          background: Container(
            color: const Color(
              0xFFF5F5F5,
            ),
          ),
        ),
      ),
    );
  }
}
