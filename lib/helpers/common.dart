import 'package:flutter/material.dart';

import '../constants/demo_style.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

space({
  final double? width,
  final double? height,
}) {
  return SizedBox(
    width: width,
    height: height,
  );
}

Widget tableText(String text) {
  return Text(
    text,
    style: DemoTextStyle.blackBoldLargeXl,
  );
}

Widget tableRowText(String text) {
  return Text(
    text,
    style: DemoTextStyle.blueBoldLarge,
  );
}

Widget appText({required String text, TextStyle? textStyle, int? maxLines}) {
  return Text(
    text,
    style: textStyle ?? DemoTextStyle.blackNormalSmall,
    maxLines: maxLines ?? 1,
  );
}

Widget detailsText(BuildContext context,
    {required String title, required String getDataText}) {
  return Padding(
    padding: const EdgeInsets.only(bottom: 10),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        appText(
          text: title,
          textStyle: DemoTextStyle.blackW700Medium,
        ),
        Expanded(
          child: appText(
            text: getDataText,
            textStyle: DemoTextStyle.blackNormalMedium,
            maxLines: 2,
          ),
        ),
      ],
    ),
  );
}
