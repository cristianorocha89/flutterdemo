import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_wrapper.dart';

class Responsive {
  smallerThanDesktop(BuildContext context) {
    return ResponsiveWrapper.of(context).isSmallerThan(DESKTOP);
  }

  smallerThanTablet(BuildContext context) {
    return ResponsiveWrapper.of(context).isSmallerThan(TABLET);
  }

  isMobile(BuildContext context) {
    return ResponsiveWrapper.of(context).isSmallerThan(MOBILE);
  }

  smallerThan4k(BuildContext context) {
    return ResponsiveWrapper.of(context).isSmallerThan("4k");
  }
}
