import 'package:flutter/material.dart';

import '../constants/demo_color.dart';
import '../constants/demo_style.dart';

Widget iconAppButton(
    {double? iconHeight,
    double? iconWidth,
    Color? color,
    String? text,
    BorderRadiusGeometry? borderRadius,
    double? iconSize,
    bool? isIconClr,
    bool? isText,
    IconData? icon,
    required Function() onTap}) {
  return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: isIconClr == true
              ? DemoColor.transparent
              : color ?? DemoColor.blue.withOpacity(0.9),
          border: Border.all(
              width: 0.5,
              color: isIconClr == true
                  ? DemoColor.transparent
                  : DemoColor.blue.withOpacity(0.9)),
          borderRadius: borderRadius ?? BorderRadius.circular(50),
        ),
        height: iconHeight ?? 35,
        width: iconWidth ?? 38,
        child: isText == true
            ? Center(
                child: Text(
                text ?? "",
                style: DemoTextStyle.whiteBoldLarge,
              ))
            : Icon(icon,
                color: isIconClr == true ? DemoColor.black : DemoColor.white,
                size: iconSize ?? 20),
      ));
}
