import 'package:demo/helpers/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import '../constants/demo_color.dart';
import '../constants/demo_style.dart';

class AppField extends StatefulWidget {
  final String name;
  final String labelText;
  final String hintText;
  final bool isNumber;
  final int? maxLength;
  final bool datePicker;
  final bool isCountry;
  final bool arrowDown;
  final bool read;
  final String? Function(String?)? validation;
  final TextEditingController controller;
  const AppField({
    this.read = false,
    Key? key,
    this.maxLength,
    required this.name,
    this.arrowDown = false,
    this.isCountry = false,
    required this.labelText,
    required this.hintText,
    required this.controller,
    this.datePicker = false,
    this.validation,
    this.isNumber = false,
  }) : super(key: key);

  @override
  State<AppField> createState() => _AppFieldState();
}

class _AppFieldState extends State<AppField> {
  DateTime selectedDate = DateTime.now();
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.labelText, style: DemoTextStyle.blackBoldLarge),
          space(height: 10),
          FormBuilderTextField(
            validator: widget.validation,
            autovalidateMode: AutovalidateMode.always,
            controller: widget.controller,
            name: widget.name,
            readOnly: widget.read,
            decoration: widget.datePicker
                ? datePickerInputDeco(
                    widget.hintText,
                    widget.isCountry,
                    widget.arrowDown,
                    widget.read,
                    suffixTap: () => _selectDate(context),
                  )
                : InputDecoration(
                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 16, horizontal: 10),
                    counter: const Offstage(),
                    hintText: widget.hintText,
                    focusedBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(
                            color: DemoColor.darkCornflowerBlue, width: 1)),
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      borderSide: BorderSide(width: 0.5),
                    ),
                  ),

            maxLength: widget.maxLength,
            keyboardType:
                widget.isNumber ? TextInputType.number : TextInputType.text,
            inputFormatters: widget.isNumber
                ? [FilteringTextInputFormatter.digitsOnly]
                : [FilteringTextInputFormatter.singleLineFormatter],
            textInputAction: TextInputAction.next,
            // validator: FormBuilderValidators.compose([
            //   FormBuilderValidators.required(),
            //   FormBuilderValidators.numeric(),
            //   FormBuilderValidators.max(70),
            // ]),
          ),
        ],
      ),
    );
  }

  InputDecoration datePickerInputDeco(
    String hint,
    bool countryPicker,
    // bool auth,
    bool read,
    bool arrowDown, {
    bool remit = true,
    Function()? suffixTap,
  }) {
    return InputDecoration(
      suffixIcon: remit
          ? GestureDetector(
              onTap: suffixTap,
              child: const Icon(
                Icons.date_range,
                color: DemoColor.grey,
                size: 25,
              ))
          : const SizedBox(),
      suffixIconColor: DemoColor.royalPurple,
      hintText: hint,
      counterText: "",
      contentPadding: const EdgeInsets.symmetric(vertical: 16, horizontal: 10),
      focusedBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          borderSide:
              BorderSide(color: DemoColor.darkCornflowerBlue, width: 1)),
      border: const OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        borderSide: BorderSide(width: 0.5),
      ),
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    var picked = await showDatePicker(
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            colorScheme: const ColorScheme.light(
              primary: DemoColor.deepLilac,
              onPrimary: DemoColor.white,
              onSurface: DemoColor.deepLilac,
            ),
            textButtonTheme: TextButtonThemeData(
              style: TextButton.styleFrom(
                primary: DemoColor.darkCornflowerBlue,
              ),
            ),
          ),
          child: child!,
        );
      },
      lastDate: DateTime(2101),
      initialDate: selectedDate,
      firstDate: DateTime(1950, 8),
      context: context,
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }

    if (widget.datePicker == true) {
      widget.controller.text =selectedDate.toIso8601String();

    }
  }
}
