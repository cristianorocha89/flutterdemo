class DemoImage {
  static const images = "assets/images/";
  static const bgImage = "${images}back_ground.svg";
}

class DemoIcon {
  static const icons = "assets/icons/";
  static const backArrow = "${icons}back_arrow.png";
}
