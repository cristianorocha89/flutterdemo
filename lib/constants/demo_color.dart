import 'package:flutter/material.dart';

class DemoColor {
  static const Color black = Color(0xFF000000);
  static const Color softBlue = Color(0xff01aed6);
  static const Color green = Colors.green;
  static const Color red = Colors.red;
  static const Color blue = Color((0xff005c99));
  static const Color white = Color((0xFff2f2f2));
  static const Color grey = Color((0xFfa6a6a6));
  static const Color transparent = Colors.transparent;
  static const Color deepLilac = Color(0xFF9854CB);
  static const Color darkCornflowerBlue = Color(0xFF1F3890);
  static const Color royalPurple = Color(0xff7A44B2);
  static const Color sonicSilver = Color(0xff757575);
}
