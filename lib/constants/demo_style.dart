import 'package:demo/constants/demo_color.dart';
import 'package:flutter/material.dart';

class DemoSize {
  static const double low = 10.0;
  static const double lower = 12.0;
  static const double small = 14.0;
  static const double medium = 16.0;
  static const double large = 18.0;
  static const double largeXl = 20.0;
  static const double largeXxl = 24.0;
}

class DemoTextStyle {
/*
 => FORMAT
   static const TextStyle demoStyle =
    TextStyle(
    color: [COLOR NAME],
    fontWeight: [FONT WEIGHT],
    fontSize:[FONT SIZE],
    fontFamily: [FONT FAMILY]
    [OTHER PROPERTIES]
   );

 =>NAME
   static const TextStyle blackBold18 =
    TextStyle(
    color:DcgColor.BLACK,
    fontWeight:FontWeight.bold,
    fontSize:18,
    fontFamily:'ROBOTO'
   );*/

  static const TextStyle blackBoldSmall = TextStyle(
    color: DemoColor.black,
    fontWeight: FontWeight.bold,
    fontSize: DemoSize.small,
  );
  static const TextStyle blackNormalSmall = TextStyle(
    color: DemoColor.black,
    fontWeight: FontWeight.normal,
    fontSize: DemoSize.small,
  );
  static const TextStyle blackBoldLarge = TextStyle(
    color: DemoColor.black,
    fontWeight: FontWeight.bold,
    fontSize: DemoSize.large,
  );

  static const TextStyle blackW700Medium = TextStyle(
    color: DemoColor.black,
    fontWeight: FontWeight.w700,
    fontSize: DemoSize.medium,
  );
  static const TextStyle blackNormalMedium = TextStyle(
    color: DemoColor.black,
    fontWeight: FontWeight.normal,
    fontSize: DemoSize.medium,
  );
  static TextStyle blackBoldLargeXl = TextStyle(
    color: DemoColor.black.withOpacity(0.7),
    fontWeight: FontWeight.bold,
    fontSize: DemoSize.largeXl,
    letterSpacing: 1,
  );
  static TextStyle blackBoldLargeXxl = TextStyle(
    color: DemoColor.black.withOpacity(0.7),
    fontWeight: FontWeight.bold,
    fontSize: DemoSize.largeXxl,
  );
  static const TextStyle whiteBoldLarge = TextStyle(
    color: DemoColor.white,
    fontWeight: FontWeight.bold,
    fontSize: DemoSize.large,
  );

  static TextStyle blueBoldLarge = const TextStyle(
    color: DemoColor.blue,
    fontWeight: FontWeight.bold,
    fontSize: DemoSize.large,
    letterSpacing: 1,
  );
}
