class DemoString {
  static String appName = 'Demo';
  static String number = 'Number';

  //-------------- Employee List
  static String employeeList = 'Employees List';
  static String emptyEmployeeList = 'Employee List is Empty';
  static String name = 'Name';
  static String email = 'Email';
  static String phone = 'Phone';
  static String action = 'Action';
  static String details = 'Details';

  //-------------- Employee Delete List
  static String deleteEmployeeList = 'Deleted Employee List';
  static String emptyDeleteEmployeeList = 'Deleted Employee List is Empty';

  //-------------- Employee Details
  static String empDetails = 'Employee Details';
  static String homeAdd = 'Home Address';

  //-------------- Employee Action
  static String deleteHeader =
      'Are you sure you want to delete this Employee Data?';
  static String delete = 'Delete';
  static String updated = 'Updated';
  static String added = 'Added';
  static String deleteMsg = ' Employee Deleted SuccessFully';
  static String updateMsg = ' Employee updated SuccessFully';
  static String addMsg = ' Employee added SuccessFully';
  static String entName = 'Enter Name';
  static String entEmail = 'Enter Email';
  static String phoneNo = 'Phone number';
  static String entPhoneNo = 'Enter Phone Number';
  static String add1 = 'Address 1';
  static String entAdd1 = 'Enter Address 1';
  static String add2 = 'Address 2';
  static String entAdd2 = 'Enter Address 2';
  static String city = 'City';
  static String entCity = 'Enter City';
  static String zipCode = 'ZIP Code';
  static String entZipCode = 'Enter ZIP Code';
  static String dateOfEmp = 'Date Of Employment';
  static String entDateOfEmp = 'Enter Date Of Employment';
  static String dateOfBrt = 'Date Of Birth';
  static String entDateOfBrt = 'Enter Date Of Birth';
  static String update = 'Update';
  static String submit = 'Submit';

//-------------- Undefine Action
  static String undefine = 'Route is not defined';
}
