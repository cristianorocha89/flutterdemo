import 'package:get/get.dart';

import 'moduls/EmployeesModule/view/employyesList.dart';
import 'moduls/main_bar_module/view/main_bar.dart';
import 'moduls/undifineModule/undefine.dart';

class Routes {
//============================================================
// ** App Named Routes **
//============================================================
  static const String mainBar = MainBar.routeName;
  static const String employees = EmployeesList.routeName;
  static const String undefined = UndefinedScreen.routeName;

  static List<GetPage<dynamic>> routes() {
    List<GetPage<dynamic>> demoRoute = [
      GetPage(name: employees, page: () => const MainBar()),
    ];
    return demoRoute;
  }

  static GetPage unDefined() {
    return GetPage(name: undefined, page: () => const UndefinedScreen());
  }
}
