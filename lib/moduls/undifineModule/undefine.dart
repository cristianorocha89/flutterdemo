import 'package:flutter/material.dart';

import '../../constants/demo_string.dart';

class UndefinedScreen extends StatelessWidget {
  static const routeName = '/undefined';
  const UndefinedScreen({Key? key}) : super(key: key);

//============================================================
// ** Flutter Build Cycle **
//============================================================

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(DemoString.undefine),
      ),
    );
  }
}
