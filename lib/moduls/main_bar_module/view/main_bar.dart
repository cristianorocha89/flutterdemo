import 'package:demo/constants/demo_color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../helpers/responsive_wrapper.dart';
import '../../EmployeesModule/view/employyesList.dart';
import '../../deleted_list_module/view/deleted_list.dart';
import '../controller/main_bar_controller.dart';

class MainBar extends StatefulWidget {
  static const routeName = "/";
  const MainBar({Key? key}) : super(key: key);

  @override
  State<MainBar> createState() => _MainBarState();
}

class _MainBarState extends State<MainBar> {
//============================================================
// ** Properties **
//============================================================
  MainBarController mainBarController = Get.put(MainBarController());

//============================================================
// ** Flutter Build Cycle **
//============================================================

  @override
  Widget build(BuildContext context) {
    return Responsive().isMobile(context) ? _mobileView(context) : _webView();
  }

//============================================================
// ** Main Widgets **
//===========================================================

  _mobileView(BuildContext context) {
    List<Widget> pages = [
      EmployeesList(),
      DeletedEmployeesList(),
    ];
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        bottomNavigationBar: menu(),
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                  child:
                      pages.elementAt(mainBarController.mobileSelectedIndex)),
            ],
          ),
        ),
      ),
    );
  }

  _webView() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Container(
            width: 80,
            decoration: BoxDecoration(
                color: DemoColor.black, borderRadius: BorderRadius.circular(5)),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Expanded(child: sideBarTitle()),
            ),
          ),
        ),
        Expanded(flex: 6, child: dashBordItem(mainBarController.selectedItem))
      ],
    );
  }

//============================================================
// ** Helper Widgets **
//============================================================

  Widget bottomBarIcon(IconData icon, bool selected) {
    return Container(
      decoration: selected
          ? const BoxDecoration(
              shape: BoxShape.circle,
              color: DemoColor.white,
            )
          : const BoxDecoration(),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Icon(icon,
              color: selected ? DemoColor.black : DemoColor.white, size: 35),
        ),
      ),
    );
  }

  Widget dashBordItem(int selected) {
    switch (selected) {
      case 0:
        return EmployeesList();
      case 1:
        return DeletedEmployeesList();

      default:
        return const Center(child: Icon(Icons.error));
    }
  }

  menu() {
    var width = MediaQuery.of(context).size.width;
    Future<void> _onItemTapped(int index) async {
      setState(() {
        mainBarController.mobileSelectedIndex = index;
      });
    }

    return SizedBox(
      width: width,
      height: 100,
      child: BottomNavigationBar(
        currentIndex: mainBarController.mobileSelectedIndex,
        type: BottomNavigationBarType.fixed,
        backgroundColor: DemoColor.black,
        unselectedFontSize: 16,
        selectedFontSize: 16,
        unselectedLabelStyle: const TextStyle(
          fontWeight: FontWeight.w600,
          color: DemoColor.white,
        ),
        selectedLabelStyle: const TextStyle(
          fontWeight: FontWeight.w600,
          color: DemoColor.white,
        ),
        selectedItemColor: DemoColor.black,
        onTap: _onItemTapped,
        items: [
          BottomNavigationBarItem(
            icon: bottomBarIcon(mainBarController.getDrawerItemList[0],
                mainBarController.mobileSelectedIndex == 0 ? true : false),
            label: "",
          ),
          BottomNavigationBarItem(
            icon: bottomBarIcon(mainBarController.getDrawerItemList[1],
                mainBarController.mobileSelectedIndex == 1 ? true : false),
            label: "",
          ),
        ],
      ),
    );
  }

  Widget sideBarTitle({bool footer = false, double? top}) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, top ?? 0, 20, 0),
      child: ListView.separated(
        shrinkWrap: true,
        itemCount: mainBarController.getDrawerItemList.length,
        itemBuilder: (BuildContext context, int i) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              titleItem(
                onTap: () {
                  setState(() {
                    mainBarController.selectedItem = i;
                  });
                },
                position: i,
                selected: mainBarController.selectedItem,
                icon: mainBarController.getDrawerItemList[i],
              ),
            ],
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return const SizedBox(
            height: 20,
          );
        },
      ),
    );
  }

  Widget titleItem({
    int? selected,
    int? position,
    Function()? onTap,
    required IconData icon,
  }) {
    return InkWell(
      hoverColor: Colors.transparent,
      focusColor: Colors.transparent,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: onTap,
      child: Container(
        decoration: selected == position
            ? const BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
              )
            : const BoxDecoration(),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(icon,
                color:
                    selected == position ? DemoColor.black : DemoColor.white),
          ),
        ),
      ),
    );

//============================================================
// ** Helper Function **
//============================================================
  }
}
