import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainBarController extends GetxController {
//============================================================
// ** Properties **
//============================================================
  var isLoading = true.obs;
  var selectedItem = 0;
  int mobileSelectedIndex = 0;
  List<IconData> getDrawerItemList = [
    Icons.person,
    Icons.delete,
  ];
}
