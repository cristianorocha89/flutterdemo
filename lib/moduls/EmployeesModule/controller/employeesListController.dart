import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

import '../../../data/repo/user_repo.dart';
import '../model/employeesModel.dart';
import '../view/employyesList.dart';

class EmployeesListController extends GetxController {
//============================================================
// ** Properties **
//============================================================
  var isLoading = true.obs;
  late bool isLastPage;
  late int pageNumber = 1;
  late RxBool error;
  late RxBool loading;

  late int count;
  int numberOfPostsPerRequest = 5;
  late RxList<Employee> employeesList;
  final nextPageTrigger = 1;

//============================================================
// ** Getx Build Cycle **
//============================================================
  @override
  void onInit() {
    pageNumber = 1;
    employeesList = <Employee>[].obs;
    isLastPage = false;
    loading = true.obs;
    error = false.obs;
    // fetchData();
    getEmployee();
    super.onInit();
  }

//============================================================
// ** Get Employee  Function **
//============================================================
  Future<void> getEmployee() async {
    try {
      isLoading(true);
      EmployeesModel? employees = (await UserRepo.getInstance()
          .getEmployeesList(EmployeesArgument(
              page: pageNumber, limit: numberOfPostsPerRequest)));
      if (employees != null) {
        count = employees.count!;
        if (kIsWeb) {
          EmployeesModel? employeesData = (await UserRepo.getInstance()
              .getEmployeesList(
                  EmployeesArgument(page: pageNumber, limit: count)));
          employeesList.addAll(employeesData!.employees!.toList());
          loading = false.obs;
        } else {
          isLastPage = (employees.employees!.length < numberOfPostsPerRequest);
          pageNumber = pageNumber + 1;
          count = employees.count!;
          employeesList.addAll(employees.employees!.toList());
          loading = false.obs;
        }
      }
    } finally {
      isLoading(false);
      loading = false.obs;
      error = true.obs;
    }
  }
}
