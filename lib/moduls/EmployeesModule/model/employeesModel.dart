// To parse this JSON data, do
//
//     final employeesModel = employeesModelFromJson(jsonString);

import 'dart:convert';

EmployeesModel employeesModelFromJson(String str) =>
    EmployeesModel.fromJson(json.decode(str));

String employeesModelToJson(EmployeesModel data) => json.encode(data.toJson());

class EmployeesModel {
  EmployeesModel({
    this.employees,
    this.count,
  });

  List<Employee>? employees;
  int? count;

  factory EmployeesModel.fromJson(Map<String, dynamic> json) => EmployeesModel(
        employees: List<Employee>.from(
            json["employees"].map((x) => Employee.fromJson(x))),
        count: json["count"],
      );

  Map<String, dynamic> toJson() => {
        "employees": List<dynamic>.from(employees!.map((x) => x.toJson())),
        "count": count,
      };
}

class Employee {
  Employee({
    this.id,
    this.deletedAt,
    this.isDeleted,
    this.dateOfBirth,
    this.dateOfEmployment,
    this.homeAddress,
    this.phoneNumber,
    this.email,
    this.name,
    this.v,
  });

  String? id;
  dynamic? deletedAt;
  bool? isDeleted;
  String? dateOfBirth;
  String? dateOfEmployment;
  HomeAddress? homeAddress;
  String? phoneNumber;
  String? email;
  String? name;
  int? v;

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
        id: json["_id"],
        deletedAt: json["deletedAt"],
        isDeleted: json["isDeleted"],
        dateOfBirth: json["dateOfBirth"],
        dateOfEmployment: json["dateOfEmployment"],
        homeAddress: HomeAddress.fromJson(json["homeAddress"]),
        phoneNumber: json["phoneNumber"],
        email: json["email"],
        name: json["name"],
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "deletedAt": deletedAt,
        "isDeleted": isDeleted,
        "dateOfBirth": dateOfBirth,
        "dateOfEmployment": dateOfEmployment,
        "homeAddress": homeAddress?.toJson(),
        "phoneNumber": phoneNumber,
        "email": email,
        "name": name,
        "__v": v,
      };
}

class HomeAddress {
  HomeAddress({
    this.addressLine2,
    this.addressLine1,
    this.zipCode,
    this.city,
    this.id,
  });

  String? addressLine2;
  String? addressLine1;
  String? zipCode;
  String? city;
  String? id;

  factory HomeAddress.fromJson(Map<String, dynamic> json) => HomeAddress(
        addressLine2: json["addressLine2"],
        addressLine1: json["addressLine1"],
        zipCode: json["ZIPCode"],
        city: json["city"],
        id: json["_id"],
      );

  Map<String, dynamic> toJson() => {
        "addressLine2": addressLine2,
        "addressLine1": addressLine1,
        "ZIPCode": zipCode,
        "city": city,
        "_id": id,
      };
}
