import 'package:demo/constants/demo_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';

import '../../../constants/demo_string.dart';
import '../../../constants/demo_style.dart';
import '../../../dialog/show_dialog.dart';
import '../../../helpers/common.dart';
import '../../../helpers/responsive_wrapper.dart';
import '../../../helpers/table_icon.dart';
import '../../EmployeeActionModule/view/employee_details.dart';
import '../../EmployeeActionModule/view/employye_action.dart';
import '../controller/employeesListController.dart';
import '../model/employeesModel.dart';

//============================================================
// ** Get Employee  Argument class **
//============================================================
class EmployeesArgument {
  final int page;
  final int limit;
  EmployeesArgument({
    required this.page,
    required this.limit,
  });
}

class EmployeesList extends StatefulWidget {
  static const routeName = "/";
  EmployeesList({Key? key}) : super(key: key);

  @override
  State<EmployeesList> createState() => _EmployeesListState();
}

class _EmployeesListState extends State<EmployeesList> {
//============================================================
// ** Properties **
//============================================================

  final EmployeesListController employeesListController =
      Get.put(EmployeesListController());
//============================================================
// ** Flutter Build Cycle **
//============================================================
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Obx(() {
      if (employeesListController.isLoading.value) {
        return const Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      } else {
        return Responsive().isMobile(context)
            ? _mobileView(height, width)
            : _webView(context, height, width);
      }
    });
  }

//============================================================
// ** Main Widgets **
//===========================================================
  Widget _mobileView(double height, double width) {
    return Scaffold(
        appBar: _mobileAppBar(height, width),
        body: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Column(
            children: [
              Expanded(
                child: employeesListController.employeesList.isEmpty
                    ? Center(
                        child: Text(DemoString.emptyEmployeeList),
                      )
                    : ListView.separated(
                        separatorBuilder: (BuildContext context, int index) {
                          return SizedBox(
                            height: height * 0.02,
                          );
                        },
                        itemCount: employeesListController.employeesList.length,
                        itemBuilder: (BuildContext context, int i) {
                          if (employeesListController.employeesList.length <
                              employeesListController.count - 1) {
                            if (i ==
                                employeesListController.employeesList.length -
                                    employeesListController.nextPageTrigger) {
                              SchedulerBinding.instance
                                  .addPostFrameCallback((_) {
                                employeesListController.getEmployee();
                              });
                            }
                          }
                          final Employee employee =
                              employeesListController.employeesList[i];
                          var phoneNumber = employee.phoneNumber?.split(" ");
                          var listPhone = phoneNumber![1];
                          return employeeDetailsCard(
                            context,
                            name: employee.name ?? "",
                            email: employee.email ?? "",
                            number: listPhone,
                            editOnTap: () {
                              showEditDialog(i, listPhone);
                            },
                            deleteOnTap: () {
                              showDeletedDialog(i);
                            },
                            moreDetailsOnTap: () {
                              showMore(i);
                            },
                          );
                        },
                      ),
              ),
            ],
          ),
        ));
  }

  Widget _webView(BuildContext context, double height, double width) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: Obx(() {
            if (employeesListController.isLoading.value) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return Column(
                children: [
                  _tableTitle(context, width, height),
                  SizedBox(
                    width: width,
                    child: PaginatedDataTable(
                      source: TableRow(
                          context: context,
                          employeesList: employeesListController,
                          employeeData: employeesListController.employeesList),
                      columns: [
                        DataColumn(label: tableText(DemoString.name)),
                        DataColumn(label: tableText(DemoString.email)),
                        DataColumn(label: tableText(DemoString.phone)),
                        DataColumn(label: tableText(DemoString.action)),
                        DataColumn(label: tableText(DemoString.details)),
                      ],
                      rowsPerPage:
                          employeesListController.numberOfPostsPerRequest,
                    ),
                  ),
                ],
              );
            }
          }),
        ),
      ],
    );
  }

//============================================================
// ** Helper Widgets **
//============================================================

  Widget employeeDetailsCard(BuildContext context,
      {required String name,
      required Function() moreDetailsOnTap,
      required String email,
      required String number,
      required Function() editOnTap,
      required Function() deleteOnTap,
      bool? isButton}) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          border: Border.all(color: DemoColor.grey),
          color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
          child: Column(children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                appText(
                    text: name.toUpperCase(),
                    textStyle: DemoTextStyle.blackBoldLarge),
                iconAppButton(
                  isIconClr: true,
                  onTap: moreDetailsOnTap,
                  icon: Icons.more_vert,
                  iconSize: 25,
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 5),
              child: Column(
                children: [
                  detailsText(context,
                      title: DemoString.email, getDataText: email),
                  space(height: 10),
                  detailsText(context,
                      title: DemoString.number, getDataText: number),
                ],
              ),
            ),
            isButton == false
                ? const SizedBox.shrink()
                : Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        iconAppButton(
                            onTap: editOnTap,
                            icon: Icons.edit,
                            borderRadius: BorderRadius.circular(10),
                            iconHeight: height * 0.055,
                            color: Colors.green,
                            iconSize: 25,
                            iconWidth: width * 0.4),
                        iconAppButton(
                            onTap: deleteOnTap,
                            icon: Icons.delete_outline_outlined,
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.red,
                            iconSize: 25,
                            iconHeight: height * 0.055,
                            iconWidth: width * 0.4)
                      ],
                    ),
                  )
          ]),
        ),
      ),
    );
  }

  AppBar _mobileAppBar(double height, double width) {
    return AppBar(
      backgroundColor: DemoColor.black,
      title: Text(DemoString.employeeList),
      actions: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: iconAppButton(
              onTap: () {
                ShowDialog.showActionEmployeeDialog(context);
              },
              icon: Icons.add,
              borderRadius: BorderRadius.circular(10),
              iconHeight: height * 0.02,
              color: DemoColor.softBlue,
              iconSize: 25,
              iconWidth: width * 0.08),
        ),
      ],
    );
  }

  Widget _tableTitle(BuildContext context, double width, double height) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(DemoString.emptyEmployeeList,
              style: DemoTextStyle.blackBoldLargeXxl),
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: iconAppButton(
              onTap: () {
                ShowDialog.showActionEmployeeDialog(context);
              },
              icon: Icons.add,
              borderRadius: BorderRadius.circular(10),
              iconWidth: width * 0.04,
              iconHeight: height * 0.055,
            ),
          )
        ],
      ),
    );
  }

//============================================================
// ** Helper Function **
//============================================================

  void showEditDialog(int i, String listPhone) {
    ShowDialog.showActionEmployeeDialog(context,
        isEdit: true,
        addEmployeeArgs: EmployeeActionArgs(
          index: i,
          employeeId: employeesListController.employeesList[i].id,
          name: employeesListController.employeesList[i].name ?? "",
          email: employeesListController.employeesList[i].email ?? "",
          phoneNumber: listPhone,
          addressLine1: employeesListController
                  .employeesList[i].homeAddress?.addressLine1 ??
              "",
          addressLine2: employeesListController
                  .employeesList[i].homeAddress?.addressLine2 ??
              "",
          city:
              employeesListController.employeesList[i].homeAddress?.city ?? "",
          zipCode:
              employeesListController.employeesList[i].homeAddress?.zipCode ??
                  "",
          dateOfBirth:
              employeesListController.employeesList[i].dateOfBirth.toString(),
          dateOfEmployment: employeesListController
              .employeesList[i].dateOfEmployment
              .toString(),
        ));
  }

  void showDeletedDialog(
    int i,
  ) {
    ShowDialog.showActionEmployeeDialog(context,
        isDelete: true,
        addEmployeeArgs: EmployeeActionArgs(
            employeeId: employeesListController.employeesList[i].id, index: i));
  }

  void showMore(int i) {
    ShowDialog.showActionEmployeeDetailsDialog(context,
        addEmployeeArgs: EmployeeDetailsArgs(
          addressLine1: employeesListController
                  .employeesList[i].homeAddress?.addressLine1 ??
              "",
          addressLine2: employeesListController
                  .employeesList[i].homeAddress?.addressLine2 ??
              "",
          city:
              employeesListController.employeesList[i].homeAddress?.city ?? "",
          zipCode:
              employeesListController.employeesList[i].homeAddress?.zipCode ??
                  "",
          dateOfBirth:
              employeesListController.employeesList[i].dateOfBirth.toString() ??
                  "",
          dateOfEmployment: employeesListController
                  .employeesList[i].dateOfEmployment
                  .toString() ??
              "",
        ));
  }
}

//============================================================
// ** Helper Class **
//============================================================

class TableRow extends DataTableSource {
  final BuildContext context;
  final EmployeesListController employeesList;
  final RxList<Employee> employeeData;
  TableRow({
    required this.context,
    required this.employeesList,
    required this.employeeData,
  });

//============================================================
// ** Flutter Build Cycle **
//===========================================================
  @override
  DataRow? getRow(int i) {
    var phoneNumber = employeeData[i].phoneNumber?.split(" ");
    var listPhone = phoneNumber![1];
    return DataRow(cells: [
      DataCell(Text(employeeData[i].name ?? "")),
      DataCell(Text(employeeData[i].email ?? "")),
      DataCell(Text(listPhone ?? "")),
      DataCell(_actionCell(i, listPhone)),
      DataCell(_moreCell(i)),
    ]);
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => employeesList.count;

  @override
  int get selectedRowCount => 0;

//============================================================
// ** Helper Widget **
//============================================================

  Widget _moreCell(int i) {
    return iconAppButton(
      onTap: () {
        showMore(i);
      },
      icon: Icons.more_vert,
      isIconClr: true,
      iconSize: 25,
    );
  }

  Widget _actionCell(int i, String listPhone) {
    return Row(
      children: [
        iconAppButton(
            onTap: () {
              showEditDialog(i, listPhone);
            },
            icon: Icons.edit),
        space(width: 10),
        iconAppButton(
            onTap: () {
              shoeDeleteDialog(i);
            },
            icon: Icons.delete_outline_outlined)
      ],
    );
  }
//============================================================
// ** Helper Function **
//============================================================

  void showEditDialog(int i, String listPhone) {
    ShowDialog.showActionEmployeeDialog(context,
        isEdit: true,
        addEmployeeArgs: EmployeeActionArgs(
          employeeId: employeeData[i].id,
          name: employeeData[i].name ?? "",
          email: employeeData[i].email ?? "",
          phoneNumber: listPhone ?? "",
          addressLine1: employeeData[i].homeAddress?.addressLine1 ?? "",
          addressLine2: employeeData[i].homeAddress?.addressLine2 ?? "",
          city: employeeData[i].homeAddress?.city ?? "",
          zipCode: employeeData[i].homeAddress?.zipCode ?? "",
          dateOfBirth: employeeData[i].dateOfBirth.toString() ?? "",
          dateOfEmployment: employeeData[i].dateOfEmployment.toString() ?? "",
        ));
  }

  void shoeDeleteDialog(int i) {
    ShowDialog.showActionEmployeeDialog(context,
        isDelete: true,
        addEmployeeArgs: EmployeeActionArgs(
          employeeId: employeeData[i].id,
        ));
  }

  void showMore(int i) {
    ShowDialog.showActionEmployeeDetailsDialog(context,
        addEmployeeArgs: EmployeeDetailsArgs(
          addressLine1: employeeData[i].homeAddress?.addressLine1 ?? "",
          addressLine2: employeeData[i].homeAddress?.addressLine2 ?? "",
          city: employeeData[i].homeAddress?.city ?? "",
          zipCode: employeeData[i].homeAddress?.zipCode ?? "",
          dateOfBirth: employeeData[i].dateOfBirth.toString() ?? "",
          dateOfEmployment: employeeData[i].dateOfEmployment.toString() ?? "",
        ));
  }
}
