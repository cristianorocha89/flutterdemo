import 'package:demo/constants/demo_color.dart';
import 'package:demo/moduls/deleted_list_module/model/deleted_list_model.dart';
import 'package:demo/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';

import '../../../constants/demo_string.dart';
import '../../../constants/demo_style.dart';
import '../../../dialog/show_dialog.dart';
import '../../../helpers/common.dart';
import '../../../helpers/responsive_wrapper.dart';
import '../../../helpers/table_icon.dart';
import '../../EmployeeActionModule/view/employee_details.dart';
import '../controller/deleted_list_controller.dart';

class DeletedEmployeesArg {
  final int page;
  final int limit;
  DeletedEmployeesArg({
    required this.page,
    required this.limit,
  });
}

class DeletedEmployeesList extends StatefulWidget {
  DeletedEmployeesList({Key? key}) : super(key: key);

  @override
  State<DeletedEmployeesList> createState() => _DeletedEmployeesListState();
}

class _DeletedEmployeesListState extends State<DeletedEmployeesList> {
//============================================================
// ** Properties **
//============================================================
  final DeletedEmployeesController deletedDeletedEmployeesList =
      Get.put(DeletedEmployeesController());

//============================================================
// ** Flutter Build Cycle **
//============================================================
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Obx(() {
      if (deletedDeletedEmployeesList.isLoading.value) {
        return const Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      } else {
        return Responsive().isMobile(context)
            ? _mobileView(height, width)
            : _webView(context, height, width);
      }
    });
  }

//============================================================
// ** Main Widgets **
//============================================================
  Widget _mobileView(double height, double width) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: DemoColor.black,
          title: Text(DemoString.deleteEmployeeList),
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Column(
            children: [
              Expanded(
                child: deletedDeletedEmployeesList.deleteEmployeesList.isEmpty
                    ? Center(
                        child: Text(DemoString.emptyDeleteEmployeeList),
                      )
                    : ListView.separated(
                        separatorBuilder: (BuildContext context, int index) {
                          appPrint(DemoString.deleteEmployeeList);
                          appPrint(deletedDeletedEmployeesList
                              .deleteEmployeesList.length
                              .toString());

                          return SizedBox(
                            height: height * 0.02,
                          );
                        },
                        itemCount: deletedDeletedEmployeesList
                            .deleteEmployeesList.length,
                        itemBuilder: (BuildContext context, int i) {
                          if (deletedDeletedEmployeesList
                                  .deleteEmployeesList.length <
                              deletedDeletedEmployeesList.count - 1) {
                            if (i ==
                                deletedDeletedEmployeesList
                                        .deleteEmployeesList.length -
                                    deletedDeletedEmployeesList
                                        .nextPageTrigger) {
                              appPrint("done");
                              SchedulerBinding.instance
                                  .addPostFrameCallback((_) {
                                deletedDeletedEmployeesList
                                    .getDeletedEmployee();
                              });
                            }
                          }
                          final DeletedEmployee employee =
                              deletedDeletedEmployeesList
                                  .deleteEmployeesList[i];

                          return employeeDetailsCard(
                            isButton: false,
                            context,
                            name: employee.name ?? "",
                            email: employee.email ?? "",
                            number: employee.phoneNumber ?? "",
                            editOnTap: () {},
                            deleteOnTap: () {},
                            moreDetailsOnTap: () {
                              moreDetails(i);
                            },
                          );
                        },
                      ),
              ),
            ],
          ),
        ));
  }

  Widget _webView(BuildContext context, double height, double width) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: Obx(() {
            if (deletedDeletedEmployeesList.isLoading.value) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return Column(
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(DemoString.deleteEmployeeList,
                            style: DemoTextStyle.blackBoldLargeXxl),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: width,
                    child: PaginatedDataTable(
                      source: TableRow(
                          context: context,
                          deleteEmployeesList: deletedDeletedEmployeesList,
                          deletedEmployeeData:
                              deletedDeletedEmployeesList.deleteEmployeesList),
                      columns: [
                        DataColumn(label: tableText(DemoString.name)),
                        DataColumn(label: tableText(DemoString.email)),
                        DataColumn(label: tableText(DemoString.phone)),
                        DataColumn(label: tableText(DemoString.details)),
                      ],
                      rowsPerPage:
                          deletedDeletedEmployeesList.numberOfPostsPerRequest,
                    ),
                  ),
                ],
              );
            }
          }),
        ),
      ],
    );
  }

//============================================================
// ** Helper Widgets **
//============================================================
  Widget employeeDetailsCard(BuildContext context,
      {required String name,
      required Function() moreDetailsOnTap,
      required String email,
      required String number,
      required Function()? editOnTap,
      required Function()? deleteOnTap,
      bool? isButton}) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          border: Border.all(color: DemoColor.grey),
          color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
          child: Column(children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                appText(
                    text: name.toUpperCase(),
                    textStyle: DemoTextStyle.blackBoldLarge),
                iconAppButton(
                  isIconClr: true,
                  onTap: moreDetailsOnTap,
                  icon: Icons.more_vert,
                  iconSize: 25,
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 5),
              child: Column(
                children: [
                  detailsText(context,
                      title: DemoString.email, getDataText: email),
                  space(height: 10),
                  detailsText(context,
                      title: DemoString.number, getDataText: number),
                ],
              ),
            ),
            isButton == false
                ? const SizedBox.shrink()
                : Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        iconAppButton(
                            onTap: editOnTap ?? () {},
                            icon: Icons.edit,
                            borderRadius: BorderRadius.circular(10),
                            iconHeight: height * 0.055,
                            color: Colors.green,
                            iconSize: 25,
                            iconWidth: width * 0.4),
                        iconAppButton(
                            onTap: deleteOnTap ?? () {},
                            icon: Icons.delete_outline_outlined,
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.red,
                            iconSize: 25,
                            iconHeight: height * 0.055,
                            iconWidth: width * 0.4)
                      ],
                    ),
                  )
          ]),
        ),
      ),
    );
  }

//============================================================
// ** Helper Function **
//============================================================
  void moreDetails(int i) {
    ShowDialog.showActionEmployeeDetailsDialog(context,
        addEmployeeArgs: EmployeeDetailsArgs(
          addressLine1: deletedDeletedEmployeesList
                  .deleteEmployeesList[i].homeAddress?.addressLine1 ??
              "",
          addressLine2: deletedDeletedEmployeesList
                  .deleteEmployeesList[i].homeAddress?.addressLine2 ??
              "",
          city: deletedDeletedEmployeesList
                  .deleteEmployeesList[i].homeAddress?.city ??
              "",
          zipCode: deletedDeletedEmployeesList
                  .deleteEmployeesList[i].homeAddress?.zipCode ??
              "",
          dateOfBirth: deletedDeletedEmployeesList
                  .deleteEmployeesList[i].dateOfBirth
                  .toString() ??
              "",
          dateOfEmployment: deletedDeletedEmployeesList
                  .deleteEmployeesList[i].dateOfEmployment
                  .toString() ??
              "",
        ));
  }
}

//============================================================
// ** Helper class **
//============================================================

class TableRow extends DataTableSource {
  final BuildContext context;
  final DeletedEmployeesController deleteEmployeesList;
  final RxList<DeletedEmployee> deletedEmployeeData;
  TableRow({
    required this.context,
    required this.deleteEmployeesList,
    required this.deletedEmployeeData,
  });
//============================================================
// ** Flutter Build Cycle **
//===========================================================
  @override
  DataRow? getRow(int i) {
    return DataRow(cells: [
      DataCell(Text(deletedEmployeeData[i].name ?? "")),
      DataCell(Text(deletedEmployeeData[i].email ?? "")),
      DataCell(Text(deletedEmployeeData[i].phoneNumber ?? "")),
      DataCell(iconAppButton(
        onTap: () {
          moreDetails(i);
        },
        icon: Icons.more_vert,
        isIconClr: true,
        iconSize: 25,
      )),
    ]);
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => deleteEmployeesList.count;

  @override
  int get selectedRowCount => 0;

//============================================================
// ** Helper Function **
//============================================================
  void moreDetails(int i) {
    ShowDialog.showActionEmployeeDetailsDialog(context,
        addEmployeeArgs: EmployeeDetailsArgs(
          addressLine1: deletedEmployeeData[i].homeAddress?.addressLine1 ?? "",
          addressLine2: deletedEmployeeData[i].homeAddress?.addressLine2 ?? "",
          city: deletedEmployeeData[i].homeAddress?.city ?? "",
          zipCode: deletedEmployeeData[i].homeAddress?.zipCode ?? "",
          dateOfBirth: deletedEmployeeData[i].dateOfBirth.toString() ?? "",
          dateOfEmployment:
              deletedEmployeeData[i].dateOfEmployment.toString() ?? "",
        ));
  }
}
