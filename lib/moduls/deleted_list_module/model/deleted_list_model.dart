// To parse this JSON data, do
//
//     final employeesModel = employeesModelFromJson(jsonString);

import 'dart:convert';

DeletedEmployeesModel employeesModelFromJson(String str) =>
    DeletedEmployeesModel.fromJson(json.decode(str));

String employeesModelToJson(DeletedEmployeesModel data) =>
    json.encode(data.toJson());

class DeletedEmployeesModel {
  DeletedEmployeesModel({
    this.employees,
    this.count,
  });

  List<DeletedEmployee>? employees;
  int? count;

  factory DeletedEmployeesModel.fromJson(Map<String, dynamic> json) =>
      DeletedEmployeesModel(
        employees: List<DeletedEmployee>.from(
            json["employees"].map((x) => DeletedEmployee.fromJson(x))),
        count: json["count"],
      );

  Map<String, dynamic> toJson() => {
        "employees": List<dynamic>.from(employees!.map((x) => x.toJson())),
        "count": count,
      };
}

class DeletedEmployee {
  DeletedEmployee({
    this.id,
    this.deletedAt,
    this.isDeleted,
    this.dateOfBirth,
    this.dateOfEmployment,
    this.homeAddress,
    this.phoneNumber,
    this.email,
    this.name,
    this.v,
  });

  String? id;
  dynamic? deletedAt;
  bool? isDeleted;
  DateTime? dateOfBirth;
  DateTime? dateOfEmployment;
  HomeAddress? homeAddress;
  String? phoneNumber;
  String? email;
  String? name;
  int? v;

  factory DeletedEmployee.fromJson(Map<String, dynamic> json) =>
      DeletedEmployee(
        id: json["_id"],
        deletedAt: json["deletedAt"],
        isDeleted: json["isDeleted"],
        dateOfBirth: DateTime.parse(json["dateOfBirth"]),
        dateOfEmployment: DateTime.parse(json["dateOfEmployment"]),
        homeAddress: HomeAddress.fromJson(json["homeAddress"]),
        phoneNumber: json["phoneNumber"],
        email: json["email"],
        name: json["name"],
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "deletedAt": deletedAt,
        "isDeleted": isDeleted,
        "dateOfBirth": dateOfBirth?.toIso8601String(),
        "dateOfEmployment": dateOfEmployment?.toIso8601String(),
        "homeAddress": homeAddress?.toJson(),
        "phoneNumber": phoneNumber,
        "email": email,
        "name": name,
        "__v": v,
      };
}

class HomeAddress {
  HomeAddress({
    this.addressLine2,
    this.addressLine1,
    this.zipCode,
    this.city,
    this.id,
  });

  String? addressLine2;
  String? addressLine1;
  String? zipCode;
  String? city;
  String? id;

  factory HomeAddress.fromJson(Map<String, dynamic> json) => HomeAddress(
        addressLine2: json["addressLine2"],
        addressLine1: json["addressLine1"],
        zipCode: json["ZIPCode"],
        city: json["city"],
        id: json["_id"],
      );

  Map<String, dynamic> toJson() => {
        "addressLine2": addressLine2,
        "addressLine1": addressLine1,
        "ZIPCode": zipCode,
        "city": city,
        "_id": id,
      };
}
