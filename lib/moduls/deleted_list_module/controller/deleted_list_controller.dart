import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

import '../../../data/repo/user_repo.dart';
import '../model/deleted_list_model.dart';
import '../view/deleted_list.dart';

class DeletedEmployeesController extends GetxController {
//============================================================
// ** Properties **
//============================================================
  var isLoading = true.obs;
  late bool isLastPage;
  late int pageNumber = 1;
  late RxBool error;
  late RxBool loading;
  late int count;
  final int numberOfPostsPerRequest = 5;
  late RxList<DeletedEmployee> deleteEmployeesList;
  final nextPageTrigger = 1;

//============================================================
// ** Getx Build Cycle **
//============================================================
  @override
  void onInit() {
    pageNumber = 1;
    deleteEmployeesList = <DeletedEmployee>[].obs;
    isLastPage = false;
    loading = true.obs;
    error = false.obs;
    // fetchData();
    getDeletedEmployee();
    super.onInit();
  }

//============================================================
// ** get Deleted Employee  Function **
//============================================================

  Future<void> getDeletedEmployee() async {
    try {
      isLoading(true);
      DeletedEmployeesModel? employees = (await UserRepo.getInstance()
          .getDeletedEmployees(DeletedEmployeesArg(
              page: pageNumber, limit: numberOfPostsPerRequest)));
      if (employees != null) {
        count = employees.count!;
        if (kIsWeb) {
          DeletedEmployeesModel? employeesData = (await UserRepo.getInstance()
              .getDeletedEmployees(
                  DeletedEmployeesArg(page: pageNumber, limit: count)));
          deleteEmployeesList.addAll(employeesData!.employees!.toList());
          loading = false.obs;
        } else {
          isLastPage = (employees.employees!.length < numberOfPostsPerRequest);
          pageNumber = pageNumber + 1;
          count = employees.count!;
          deleteEmployeesList.addAll(employees.employees!.toList());
          loading = false.obs;
        }
      }
    } finally {
      isLoading(false);
      loading = false.obs;
      error = true.obs;
    }
  }
}
