// To parse this JSON data, do
//
//     final employeeActionModel = employeeActionModelFromJson(jsonString);

import 'dart:convert';

EmployeeActionModel employeeActionModelFromJson(String str) =>
    EmployeeActionModel.fromJson(json.decode(str));

String employeeActionModelToJson(EmployeeActionModel data) =>
    json.encode(data.toJson());

class EmployeeActionModel {
  EmployeeActionModel({
    this.deletedAt,
    this.isDeleted,
    this.dateOfBirth,
    this.dateOfEmployment,
    this.homeAddress,
    this.phoneNumber,
    this.email,
    this.name,
    this.id,
    this.v,
  });

  dynamic? deletedAt;
  bool? isDeleted;
  DateTime? dateOfBirth;
  DateTime? dateOfEmployment;
  HomeAddress? homeAddress;
  String? phoneNumber;
  String? email;
  String? name;
  String? id;
  int? v;

  factory EmployeeActionModel.fromJson(Map<String, dynamic> json) =>
      EmployeeActionModel(
        deletedAt: json["deletedAt"],
        isDeleted: json["isDeleted"],
        dateOfBirth: DateTime.parse(json["dateOfBirth"]),
        dateOfEmployment: DateTime.parse(json["dateOfEmployment"]),
        homeAddress: HomeAddress.fromJson(json["homeAddress"]),
        phoneNumber: json["phoneNumber"],
        email: json["email"],
        name: json["name"],
        id: json["_id"],
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "deletedAt": deletedAt,
        "isDeleted": isDeleted,
        "dateOfBirth":
            "${dateOfBirth?.year.toString().padLeft(4, '0')}-${dateOfBirth?.month.toString().padLeft(2, '0')}-${dateOfBirth?.day.toString().padLeft(2, '0')}",
        "dateOfEmployment":
            "${dateOfEmployment?.year.toString().padLeft(4, '0')}-${dateOfEmployment?.month.toString().padLeft(2, '0')}-${dateOfEmployment?.day.toString().padLeft(2, '0')}",
        "homeAddress": homeAddress?.toJson(),
        "phoneNumber": phoneNumber,
        "email": email,
        "name": name,
        "_id": id,
        "__v": v,
      };
}

class HomeAddress {
  HomeAddress({
    this.addressLine2,
    this.addressLine1,
    this.zipCode,
    this.city,
    this.id,
  });

  String? addressLine2;
  String? addressLine1;
  String? zipCode;
  String? city;
  String? id;

  factory HomeAddress.fromJson(Map<String, dynamic> json) => HomeAddress(
        addressLine2: json["addressLine2"],
        addressLine1: json["addressLine1"],
        zipCode: json["ZIPCode"],
        city: json["city"],
        id: json["_id"],
      );

  Map<String, dynamic> toJson() => {
        "addressLine2": addressLine2,
        "addressLine1": addressLine1,
        "ZIPCode": zipCode,
        "city": city,
        "_id": id,
      };
}
