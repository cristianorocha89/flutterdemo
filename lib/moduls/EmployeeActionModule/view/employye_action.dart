import 'package:demo/constants/demo_string.dart';
import 'package:demo/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';

import '../../../constants/demo_color.dart';
import '../../../constants/demo_style.dart';
import '../../../helpers/app_field.dart';
import '../../../helpers/common.dart';
import '../../../helpers/responsive_wrapper.dart';
import '../../../helpers/table_icon.dart';
import '../../EmployeesModule/controller/employeesListController.dart';
import '../controller/employee_action_controller.dart';

//============================================================
// ** Employee Action Argument class **
//============================================================
class EmployeeActionArgs {
  String? name;
  String? email;
  String? phoneNumber;
  String? city;
  String? zipCode;
  String? addressLine1;
  String? addressLine2;
  String? dateOfEmployment;
  String? dateOfBirth;
  String? employeeId;
  int? index;

  EmployeeActionArgs(
      {this.name,
      this.email,
      this.phoneNumber,
      this.dateOfEmployment,
      this.dateOfBirth,
      this.city,
      this.zipCode,
      this.addressLine1,
      this.addressLine2,
      this.employeeId,
      this.index});
}

class EmployeesAction extends StatefulWidget {
  bool isEdit;
  bool isDelete;
  final EmployeeActionArgs? args;
  EmployeesAction({
    Key? key,
    this.isEdit = false,
    this.isDelete = false,
    this.args,
  }) : super(key: key);

  @override
  State<EmployeesAction> createState() => _EmployeesActionState();
}

class _EmployeesActionState extends State<EmployeesAction> {
//============================================================
// ** Properties **
//============================================================
  final EmployeesActionController employeeActionList =
      Get.put(EmployeesActionController());

  final EmployeesListController employeesListController =
      Get.put(EmployeesListController());
  final _formKey = GlobalKey<FormBuilderState>();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController cityController = TextEditingController();
  final TextEditingController zipCodeController = TextEditingController();
  final TextEditingController address1Controller = TextEditingController();
  final TextEditingController address2Controller = TextEditingController();
  final TextEditingController dateOfEmpController = TextEditingController();
  final TextEditingController dateOfBrtController = TextEditingController();

//============================================================
// ** Flutter Build Cycle **
//============================================================

  @override
  void initState() {
    getEmployeeData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Center(
      child: widget.isDelete
          ? _deleteEmployeeDialog(context, width, height)
          : _addUpdateDialog(context, width, height),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

//============================================================
// ** Main Widgets **
//============================================================

  Widget _addUpdateDialog(BuildContext context, double width, double height) {
    return FormBuilder(
      key: _formKey,
      child: Container(
        width: Responsive().isMobile(context) ? width * 0.95 : width * 0.4,
        height: Responsive().isMobile(context) ? height * 0.9 : height * 0.95,
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(15))),
        child: ScrollConfiguration(
          behavior: ScrollConfiguration.of(context).copyWith(scrollbars: false),
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 50, vertical: 30),
                child: Center(
                  child: Column(
                    children: [
                      _fieldBar(height, context, width),
                      _actionButton(height, context, width)
                    ],
                  ),
                )),
          ),
        ),
      ),
    );
  }

  Widget _deleteEmployeeDialog(
      BuildContext context, double width, double height) {
    return Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(15))),
      width: Responsive().isMobile(context) ? width * 0.95 : width * 0.35,
      height: Responsive().isMobile(context) ? 250 : 200,
      child: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: Column(
                children: [
                  Text(
                      textAlign: TextAlign.center,
                      DemoString.deleteHeader,
                      style: DemoTextStyle.blackBoldLargeXxl),
                  space(height: height * 0.05),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      iconAppButton(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          icon: Icons.cancel,
                          borderRadius: BorderRadius.circular(10),
                          iconHeight: height * 0.055,
                          color: Colors.grey,
                          iconSize: 25,
                          iconWidth: Responsive().isMobile(context)
                              ? width * 0.4
                              : width * 0.15),
                      _deleteActionButton(context, height, width)
                    ],
                  ),
                ],
              ),
            )),
      ),
    );
  }

//============================================================
// ** Helper Widgets **
//============================================================

  Widget _fieldBar(double height, BuildContext context, double width) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AppField(
          validation: FormBuilderValidators.compose([
            FormBuilderValidators.required(),
          ]),
          hintText: DemoString.entName,
          name: DemoString.name,
          labelText: DemoString.name,
          controller: nameController,
        ),
        AppField(
          validation: FormBuilderValidators.compose([
            FormBuilderValidators.email(),
            FormBuilderValidators.required(),
          ]),
          hintText: DemoString.entEmail,
          name: DemoString.email,
          labelText: DemoString.email,
          controller: emailController,
        ),
        AppField(
          validation: FormBuilderValidators.compose([
            FormBuilderValidators.required(),
          ]),
          isNumber: true,
          maxLength: 10,
          hintText: DemoString.entPhoneNo,
          name: DemoString.phoneNo,
          labelText: DemoString.phoneNo,
          controller: phoneController,
        ),
        AppField(
          validation: FormBuilderValidators.compose([
            FormBuilderValidators.required(),
          ]),
          hintText: DemoString.entAdd1,
          name: DemoString.add1,
          labelText: DemoString.add1,
          controller: address1Controller,
        ),
        AppField(
          validation: FormBuilderValidators.compose([
            FormBuilderValidators.required(),
          ]),
          hintText: DemoString.entAdd2,
          name: DemoString.add2,
          labelText: DemoString.add2,
          controller: address2Controller,
        ),
        AppField(
          validation: FormBuilderValidators.compose([
            FormBuilderValidators.required(),
          ]),
          hintText: DemoString.entCity,
          name: DemoString.city,
          labelText: DemoString.city,
          controller: cityController,
        ),
        AppField(
          validation: FormBuilderValidators.compose([
            FormBuilderValidators.numeric(),
            FormBuilderValidators.required(),
          ]),
          hintText: DemoString.entZipCode,
          name: DemoString.zipCode,
          maxLength: 5,
          isNumber: true,
          labelText: DemoString.zipCode,
          controller: zipCodeController,
        ),
        AppField(
          validation: FormBuilderValidators.compose([
            FormBuilderValidators.required(),
          ]),
          read: true,
          datePicker: true,
          hintText: DemoString.entDateOfEmp,
          name: DemoString.dateOfEmp,
          labelText: DemoString.dateOfEmp,
          controller: dateOfEmpController,
        ),
        AppField(
          validation: FormBuilderValidators.compose([
            FormBuilderValidators.required(),
          ]),
          read: true,
          datePicker: true,
          hintText: DemoString.entDateOfBrt,
          name: DemoString.dateOfBrt,
          labelText: DemoString.dateOfBrt,
          controller: dateOfBrtController,
        ),
      ],
    );
  }

  Widget _actionButton(double height, BuildContext context, double width) {
    return iconAppButton(
      isText: true,
      text: widget.isEdit
          ? DemoString.update.toUpperCase()
          : DemoString.submit.toUpperCase(),
      color: DemoColor.softBlue,
      borderRadius: BorderRadius.circular(10),
      iconHeight: height * 0.055,
      iconWidth: Responsive().isMobile(context) ? width * 0.4 : width * 0.1,
      onTap: () {
        if (_formKey.currentState?.saveAndValidate() ?? false) {
          if (widget.isEdit == true) {
            updateEmployeeData();
          } else {
            addEmployeeData();
          }
          Navigator.pop(context);
        }
      },
    );
  }

  Widget _deleteActionButton(
      BuildContext context, double height, double width) {
    return iconAppButton(
        onTap: () {
          deleteEmployeeData();
        },
        icon: Icons.delete_outline_outlined,
        borderRadius: BorderRadius.circular(10),
        color: Colors.red,
        iconSize: 25,
        iconHeight: height * 0.055,
        iconWidth: Responsive().isMobile(context) ? width * 0.4 : width * 0.15);
  }
//============================================================
// ** Helper Function **
//============================================================

  void getEmployeeData() {
    if (widget.isEdit == true) {
      nameController.text = widget.args!.name.toString();
      emailController.text = widget.args!.email.toString();
      phoneController.text = widget.args!.phoneNumber.toString();
      cityController.text = widget.args!.city.toString();
      zipCodeController.text = widget.args!.zipCode.toString();
      address1Controller.text = widget.args!.addressLine1.toString();
      address2Controller.text = widget.args!.addressLine2.toString();
      dateOfEmpController.text = widget.args!.dateOfEmployment.toString();
      dateOfBrtController.text = widget.args!.dateOfBirth.toString();
      appPrint("get data");
      appPrint(
          dateOfEmpController.text = widget.args!.dateOfEmployment.toString());
      appPrint(dateOfBrtController.text = widget.args!.dateOfBirth.toString());
    }
  }

  void addEmployeeData() {
    employeeActionList
        .addEmployee(EmployeeActionArgs(
            email: emailController.text,
            name: nameController.text,
            city: cityController.text,
            phoneNumber: phoneController.text,
            addressLine1: address1Controller.text,
            addressLine2: address2Controller.text,
            dateOfBirth: dateOfBrtController.text,
            dateOfEmployment: dateOfEmpController.text,
            zipCode: zipCodeController.text))
        .then((value) {
      employeesListController.employeesList.clear();
      employeesListController.pageNumber = 1;
      employeesListController.numberOfPostsPerRequest = 5;
      employeesListController.getEmployee().then((value) => {
            Navigator.pop(context),
            Get.snackbar(DemoString.added, DemoString.addMsg,
                backgroundColor: DemoColor.green)
          });
    });
  }

  void updateEmployeeData() {
    employeeActionList
        .updateEmployee(EmployeeActionArgs(
            employeeId: widget.args!.employeeId,
            email: emailController.text,
            name: nameController.text,
            city: cityController.text,
            phoneNumber: phoneController.text,
            addressLine1: address1Controller.text,
            addressLine2: address2Controller.text,
            dateOfBirth: dateOfBrtController.text,
            dateOfEmployment: dateOfEmpController.text,
            zipCode: zipCodeController.text))
        .then((value) {
      employeesListController.employeesList.clear();
      employeesListController.pageNumber = 1;
      employeesListController.numberOfPostsPerRequest = 5;
      employeesListController.getEmployee().then((value) => {
            Navigator.pop(context),
            Get.snackbar(DemoString.updated.toUpperCase(), DemoString.updateMsg,
                backgroundColor: DemoColor.green)
          });
    });
  }

  void deleteEmployeeData() {
    employeeActionList
        .deleteEmployee(EmployeeActionArgs(
      employeeId: widget.args!.employeeId,
    ))
        .then((value) {
      employeesListController.employeesList.clear();
      employeesListController.pageNumber = 1;
      employeesListController.numberOfPostsPerRequest = 5;
      employeesListController.getEmployee().then((value) => {
            Navigator.pop(context),
            Get.snackbar(DemoString.delete, DemoString.deleteMsg,
                backgroundColor: DemoColor.green)
          });
    });
  }
}
