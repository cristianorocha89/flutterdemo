import 'package:demo/constants/demo_color.dart';
import 'package:demo/constants/demo_style.dart';
import 'package:flutter/material.dart';

import '../../../constants/demo_string.dart';
import '../../../helpers/common.dart';
import '../../../helpers/responsive_wrapper.dart';

class EmployeeDetailsArgs {
  String? name;
  String? email;
  String? phoneNumber;
  String? city;
  String? zipCode;
  String? addressLine1;
  String? addressLine2;
  String? dateOfEmployment;
  String? dateOfBirth;
  String? employeeId;

  EmployeeDetailsArgs(
      {this.name,
      this.email,
      this.phoneNumber,
      this.dateOfEmployment,
      this.dateOfBirth,
      this.city,
      this.zipCode,
      this.addressLine1,
      this.addressLine2,
      this.employeeId});
}

class EmployeeDetails extends StatelessWidget {
  final EmployeeDetailsArgs? args;
  const EmployeeDetails({Key? key, this.args}) : super(key: key);
//============================================================
// ** Properties **
//============================================================

//============================================================
// ** Flutter Build Cycle **
//============================================================

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return _mainBody(context, width);
  }

//============================================================
// ** Main Widgets **
//============================================================
  Center _mainBody(BuildContext context, double width) {
    return Center(
      child: Container(
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(15))),
        width: Responsive().isMobile(context) ? width * 0.9 : width * 0.4,
        height: 220,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: ScrollConfiguration(
            behavior:
                ScrollConfiguration.of(context).copyWith(scrollbars: false),
            child: SingleChildScrollView(
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: appText(
                          text: DemoString.employeeList,
                          textStyle: DemoTextStyle.blueBoldLarge),
                    ),
                    const Divider(
                        height: 20, color: DemoColor.darkCornflowerBlue),
                    detailsText(context,
                        title: DemoString.homeAdd,
                        getDataText:
                            "${args!.addressLine1.toString()}, ${args!.addressLine2}, ${args!.city}, ${args!.zipCode}"),
                    detailsText(context,
                        title: DemoString.dateOfBrt,
                        getDataText: args!.dateOfBirth.toString()),
                    detailsText(context,
                        title: DemoString.dateOfEmp,
                        getDataText: args!.dateOfEmployment.toString()),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

//============================================================
// ** Helper Widgets **
//============================================================

//============================================================
// ** Helper Function **
//============================================================
}
