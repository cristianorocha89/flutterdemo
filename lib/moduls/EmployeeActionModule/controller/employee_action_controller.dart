import 'package:get/get.dart';

import '../../../data/repo/user_repo.dart';
import '../model/employee_action_model.dart';
import '../view/employye_action.dart';

class EmployeesActionController extends GetxController {
//============================================================
// ** Properties **
//============================================================
  var isLoading = true.obs;
  EmployeeActionModel addEmployeesModel = EmployeeActionModel();

//============================================================
// ** Add Employee  Function **
//============================================================
  Future<EmployeeActionModel> addEmployee(
      EmployeeActionArgs addEmployeeArgs) async {
    try {
      isLoading(true);
      EmployeeActionModel? employees =
          (await UserRepo.getInstance().addEmployees(addEmployeeArgs));
      if (employees != null) {
        addEmployeesModel = employees;
        return addEmployeesModel;
      }
      return EmployeeActionModel();
    } finally {
      isLoading(false);
    }
  }

//============================================================
// ** Update Employee  Function **
//============================================================

  Future<void> updateEmployee(EmployeeActionArgs addEmployeeArgs) async {
    try {
      isLoading(true);
      EmployeeActionModel? employees =
          (await UserRepo.getInstance().updateEmployees(addEmployeeArgs));
      if (employees != null) {
        addEmployeesModel = employees;
      }
    } finally {
      isLoading(false);
    }
  }

//============================================================
// ** Delete Employee  Function **
//============================================================

  Future<void> deleteEmployee(EmployeeActionArgs addEmployeeArgs) async {
    try {
      isLoading(true);
      EmployeeActionModel? employees =
          (await UserRepo.getInstance().deleteEmployees(addEmployeeArgs));
      if (employees != null) {
        addEmployeesModel = employees;
      }
    } finally {
      isLoading(false);
    }
  }
}
